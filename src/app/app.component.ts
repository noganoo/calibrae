import { MatIconRegistry } from '@angular/material';
import { Component, HostBinding, OnInit } from '@angular/core';
import { OverlayContainer } from '@angular/cdk/overlay';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  viewProviders: [MatIconRegistry]
})
export class AppComponent implements OnInit {
  title = 'Calibrae PoC';
  copyright = 'Calibrae © 2017';
  isDarkTheme: boolean;
  themeIconName = 'wb_sunny';
  themeTooltip = 'Click to toggle to night mode';
  displayWidth: number;

  constructor(
    public overlayContainer: OverlayContainer,
    iconReg: MatIconRegistry,
    sanitizer: DomSanitizer
  ) {
    iconReg.addSvgIcon(
      'gitlab',
      sanitizer.bypassSecurityTrustResourceUrl('../assets/gitlab.svg')
    );
    this.isDarkTheme = false;
    this.overlayContainer.getContainerElement().classList.add('bright-theme');
    this.componentCssClass = 'bright-theme';
  }
  @HostBinding('class') componentCssClass;
  changeTheme(): void {
    if (this.isDarkTheme) {
      this.isDarkTheme = false;
      this.overlayContainer.getContainerElement().classList.add('bright-theme');
      this.componentCssClass = 'bright-theme';
      this.themeIconName = 'wb_sunny';
      this.themeTooltip = 'Click to toggle to night mode';
    } else {
      this.isDarkTheme = true;
      this.overlayContainer.getContainerElement().classList.add('dark-theme');
      this.componentCssClass = 'dark-theme';
      this.themeIconName = 'brightness_3';
      this.themeTooltip = 'Click to toggle to day mode';
    }
  }
  ngOnInit() {}
}
